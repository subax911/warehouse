import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IFloor, IFloorSection } from '../models/floor.model';
import { IProduct } from '../models/product.model';

const FLOORS: IFloor[] = [
  {
    name: 'Floor 1',
    number: 1,
    sections: [
      {
        name: 'Section 1',
        number: 1,
        products: []
      },
      {
        name: 'Section 2',
        number: 2,
        products: []
      },
      {
        name: 'Section 3',
        number: 3,
        products: []
      },
    ]
  },
  {
    name: 'Floor 2',
    number: 2,
    sections: [
      {
        name: 'Section 4',
        number: 4,
        products: []
      },
      {
        name: 'Section 5',
        number: 5,
        products: []
      },
      {
        name: 'Section 6',
        number: 6,
        products: []
      },
    ]
  },
  {
    name: 'Floor 3',
    number: 3,
    sections: [
      {
        name: 'Section 7',
        number: 7,
        products: []
      },
      {
        name: 'Section 8',
        number: 8,
        products: []
      },
      {
        name: 'Section 9',
        number: 9,
        products: []
      },
    ]
  },
];

@Injectable({providedIn: 'root'})
export class WarehouseService {
  private floors$ = new BehaviorSubject<IFloor[]>(FLOORS);
  private products$ = new BehaviorSubject<IProduct[]>([]);
  filteredProducts$ = new BehaviorSubject<IProduct[]>([]);

  search(term: string): Observable<IProduct[]> {
    const { value } = this.products$;
    const filtered = value.filter(item => {
      return item.code.toLowerCase().includes(term.toLowerCase()) ||
             item.floorName.toLowerCase().includes(term.toLowerCase()) ||
             item.sectionName.toLowerCase().includes(term.toLowerCase());
    });
    this.filteredProducts$.next(filtered);
    return this.filteredProducts$.asObservable();
  }

  getFloors(): Observable<IFloor[]> {
    return this.floors$.asObservable();
  }

  edit(product: IProduct): Observable<IProduct> {
    const { value } = this.products$;
    const found = value.find(item => item.code === product.code);
    // @ts-ignore
    Object.keys(found).forEach(key => found[key] = product[key]);
    return of(product);
  }

  addProduct(product: IProduct): Observable<IProduct> {
    const section = this.getSection(product.sectionNumber, product.floorNumber);
    section?.products.push(product);
    const { value } = this.products$;
    value.push(product);
    this.products$.next(value);
    return of(product);
  }

  /**
   * These two functions should prolly be api calls or simulated api calls in this case,
   * and also route params and not query params, but for the sake of time I implemented it this way
   */
  getFloor(number: number): IFloor | undefined {
    const { value } = this.floors$;
    return value.find(item => item.number === number);
  }

  getSection(sectionNumber: number, floorNumber: number): IFloorSection | undefined {
    const floor = this.getFloor(floorNumber);
    return floor?.sections.find(item => item.number === sectionNumber);
  }
}
