export interface IProduct {
  code: string;
  quantity: number;
  floorNumber: number;
  sectionNumber: number;
  floorName: string;
  sectionName: string;
}
