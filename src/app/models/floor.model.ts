import { IProduct } from './product.model';

interface IBaseModel {
  name: string;
  number: number;
}

export interface IFloorSection extends IBaseModel {
  products: IProduct[];
}

export interface IFloor extends IBaseModel {
  sections: IFloorSection[];
}
