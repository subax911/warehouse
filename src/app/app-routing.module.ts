import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'warehouse',
    loadChildren: () => import('./pages/warehouse/warehouse.module').then(mod => mod.WarehouseModule)
  },
  {
    path: '',
    redirectTo: 'warehouse',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'warehouse',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
