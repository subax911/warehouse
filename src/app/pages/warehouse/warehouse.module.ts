import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WarehouseRoutingModule } from './warehouse-routing.module';
import { WarehouseComponent } from './warehouse.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    WarehouseComponent,
  ],
  imports: [
    CommonModule,
    WarehouseRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class WarehouseModule { }
