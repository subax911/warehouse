import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable, timer } from 'rxjs';
import { debounce, switchMap } from 'rxjs/operators';
import { IFloor, IFloorSection } from 'src/app/models/floor.model';
import { IProduct } from 'src/app/models/product.model';
import { WarehouseService } from 'src/app/services/warehouse.service';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.scss']
})
export class WarehouseComponent implements OnInit {
  floors$: Observable<IFloor[]> | undefined;
  selectedFloor: IFloor | undefined;
  selectedSection: IFloorSection | undefined;
  isAddMode = false;
  isEditMode = false;

  filter = new FormControl(null);

  form = new FormGroup({
    code: new FormControl(null, [Validators.required, Validators.pattern(/[A-Z]{2,4}\s[0-9]{4,6}/)]),
    quantity: new FormControl(null, [Validators.required]),
    floorNumber: new FormControl(null, [Validators.required]),
    sectionNumber: new FormControl(null, [Validators.required])
  });

  constructor(
    public readonly warehouseService: WarehouseService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.floors$ = this.warehouseService.getFloors();
    this.route.queryParams.subscribe(params => {
      this.selectedFloor = this.warehouseService.getFloor(Number(params.floorNumber));
      this.selectedSection = this.warehouseService.getSection(Number(params.sectionNumber), Number(params.floorNumber));
    });
    this.filter.valueChanges
      .pipe(
        debounce(() => timer(1000)),
        switchMap(val => this.warehouseService.search(val))
      ).subscribe();
  }

  save(): void {
    const section = this.warehouseService.getSection(Number(this.form.value.sectionNumber), Number(this.form.value.floorNumber)) as IFloorSection;
    const floor = this.warehouseService.getFloor(Number(this.form.value.floorNumber)) as IFloor;
    const product: IProduct = {
      code: this.form.getRawValue().code,
      floorNumber: floor.number,
      quantity: this.form.value.quantity,
      sectionNumber: section.number,
      sectionName: section.name,
      floorName: floor.name,
    };
    if (this.isEditMode) {
      this.warehouseService.edit(product).subscribe();
    } else {
      this.warehouseService.addProduct(product).subscribe();
    }
    this.disableFormMode();
    this.form.reset();
  }

  edit(product: IProduct): void {
    this.controls.code?.setValue(product.code);
    this.controls.floorNumber?.setValue(product.floorNumber);
    this.controls.quantity?.setValue(product.quantity);
    this.controls.sectionNumber?.setValue(product.sectionNumber);
    this.controls.code?.disable();
    this.isEditMode = true;
  }

  select(floor: IFloor, section?: IFloorSection): void {
    this.disableFormMode();
    const queryParams: any = {
      floorNumber: floor.number,
    }
    if (section) {
      queryParams.sectionNumber = section.number;
    }
    this.router.navigate(['/warehouse/products'], {queryParams});
  }

  get floor(): IFloor | undefined {
    return this.warehouseService.getFloor(Number(this.form.value.floorNumber));
  }

  get controls() {
    return {
      code: this.form.get('code'),
      floorNumber: this.form.get('floorNumber'),
      quantity: this.form.get('quantity'),
      sectionNumber: this.form.get('sectionNumber'),
    };
  }

  disableFormMode(): void {
    this.controls.code?.enable();
    this.form.reset();
    this.isAddMode = false;
    this.isEditMode = false;
  }
}
